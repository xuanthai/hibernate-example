package com.thaibui.spring.main;

import com.thaibui.spring.entities.Employee;
import com.thaibui.spring.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class HibernateAnnotationMain {
    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.setFirstName("Bui");
        emp.setLastName("Thai");
        emp.setSalary(5);

        //Get Session
        SessionFactory sessionFactory = HibernateUtil.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        //start transaction
        session.beginTransaction();
        //Save the Model object
        session.save(emp);
        //Commit transaction
        session.getTransaction().commit();
        System.out.println("Employee ID="+emp.getId());

        //terminate session factory, otherwise program won't end
        sessionFactory.close();
    }
}
