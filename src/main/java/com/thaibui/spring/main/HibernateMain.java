package com.thaibui.spring.main;

import com.thaibui.spring.entities.Employee;
import com.thaibui.spring.util.HibernateUtil;
import org.hibernate.Session;

public class HibernateMain {
    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.setFirstName("Thai");
        emp.setLastName("Xuan");
        emp.setSalary(20);

        //Get Session
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        //start transaction
        session.beginTransaction();
        //Save the Model object
        session.save(emp);
        //Commit transaction
        session.getTransaction().commit();
        System.out.println("Employee ID="+emp.getId());

        //terminate session factory, otherwise program won't end
        HibernateUtil.getSessionFactory().close();
    }
}
